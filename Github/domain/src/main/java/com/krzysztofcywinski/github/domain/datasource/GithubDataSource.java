package com.krzysztofcywinski.github.domain.datasource;

import com.krzysztofcywinski.github.domain.model.Repo;

import java.util.List;

import rx.Observable;

public interface GithubDataSource {

    Observable<List<Repo>> getOrganisationRepos(String organisation);
}
