package com.krzysztofcywinski.github.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor(suppressConstructorProperties = true)
@Getter
@Setter
public class Repo implements Parcelable {

    private Integer id;

    private RepoOwner owner;

    private String name;

    private String fullName;

    private String description;

    private Boolean _private;

    private String url;

    private String gitUrl;

    private String homepage;

    private Integer watchersCount;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeParcelable(this.owner, flags);
        dest.writeString(this.name);
        dest.writeString(this.fullName);
        dest.writeString(this.description);
        dest.writeValue(this._private);
        dest.writeString(this.url);
        dest.writeString(this.gitUrl);
        dest.writeString(this.homepage);
        dest.writeValue(this.watchersCount);
    }

    protected Repo(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.owner = in.readParcelable(RepoOwner.class.getClassLoader());
        this.name = in.readString();
        this.fullName = in.readString();
        this.description = in.readString();
        this._private = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.url = in.readString();
        this.gitUrl = in.readString();
        this.homepage = in.readString();
        this.watchersCount = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<Repo> CREATOR = new Creator<Repo>() {
        @Override
        public Repo createFromParcel(Parcel source) {
            return new Repo(source);
        }

        @Override
        public Repo[] newArray(int size) {
            return new Repo[size];
        }
    };
}
