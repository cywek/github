package com.krzysztofcywinski.github.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor(suppressConstructorProperties = true)
@Getter
@Setter
public class RepoOwner implements Parcelable {

    private String login;

    private Integer id;

    private String avatarUrl;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.login);
        dest.writeValue(this.id);
        dest.writeString(this.avatarUrl);
    }

    public RepoOwner() {
    }

    protected RepoOwner(Parcel in) {
        this.login = in.readString();
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.avatarUrl = in.readString();
    }

    public static final Creator<RepoOwner> CREATOR = new Creator<RepoOwner>() {
        @Override
        public RepoOwner createFromParcel(Parcel source) {
            return new RepoOwner(source);
        }

        @Override
        public RepoOwner[] newArray(int size) {
            return new RepoOwner[size];
        }
    };
}
