package com.krzysztofcywinski.github.data.source;

import com.krzysztofcywinski.github.data.entity.RepoDto;
import com.krzysztofcywinski.github.data.network.provider.GithubServiceProvider;
import com.krzysztofcywinski.github.data.network.service.GithubService;
import com.krzysztofcywinski.github.domain.datasource.GithubDataSource;
import com.krzysztofcywinski.github.domain.model.Repo;

import java.util.List;

import rx.Observable;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class GithubDataSourceImpl implements GithubDataSource {

    private static final String API_BASE_URL = "https://api.github.com";
    private GithubService service;

    public GithubDataSourceImpl() {
        service = GithubServiceProvider.
                newInstance(
                        API_BASE_URL)
                .get();
    }

    @Override
    public Observable<List<Repo>> getOrganisationRepos(String organisation) {
        Observable<List<RepoDto>> observable = service.getOrganisationRepos(organisation);
        return observable.map(new Func1<List<RepoDto>, List<Repo>>() {
            @Override
            public List<Repo> call(List<RepoDto> repoDtos) {
                return RepoDto.toRepo(repoDtos);
            }
        }).
        subscribeOn(Schedulers.io());
    }

}
