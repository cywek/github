package com.krzysztofcywinski.github.data.network.service;

import com.krzysztofcywinski.github.data.entity.RepoDto;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

public interface GithubService {

    @GET("/orgs/{org}/repos")
    Observable<List<RepoDto>> getOrganisationRepos(@Path("org") String organisation);

}
