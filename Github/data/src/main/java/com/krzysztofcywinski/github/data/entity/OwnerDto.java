package com.krzysztofcywinski.github.data.entity;

import com.krzysztofcywinski.github.domain.model.RepoOwner;
import com.squareup.moshi.Json;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor(suppressConstructorProperties = true)
@Getter
@Setter
public class OwnerDto {

    @Json(name = "login")
    private String login;

    @Json(name = "id")
    private Integer id;

    @Json(name = "avatar_url")
    private String avatarUrl;

    @Json(name = "gravatar_id")
    private String gravatarId;

    @Json(name = "url")
    private String url;

    @Json(name = "html_url")
    private String htmlUrl;

    @Json(name = "followers_url")
    private String followersUrl;

    @Json(name = "following_url")
    private String followingUrl;

    @Json(name = "gists_url")
    private String gistsUrl;

    @Json(name = "starred_url")
    private String starredUrl;

    @Json(name = "subscriptions_url")
    private String subscriptionsUrl;

    @Json(name = "organizations_url")
    private String organizationsUrl;

    @Json(name = "repos_url")
    private String reposUrl;

    @Json(name = "events_url")
    private String eventsUrl;

    @Json(name = "received_events_url")
    private String receivedEventsUrl;

    @Json(name = "type")
    private String type;

    @Json(name = "site_admin")
    private Boolean siteAdmin;

    public static RepoOwner toRepoOwner(OwnerDto ownerDto) {
        return new RepoOwner(
                ownerDto.getLogin(),
                ownerDto.getId(),
                ownerDto.getAvatarUrl());
    }

}
