package com.krzysztofcywinski.github.data.network.provider;

import android.util.Log;

import com.krzysztofcywinski.github.data.network.interceptor.GithubInterceptor;
import com.krzysztofcywinski.github.data.network.service.GithubService;
import com.squareup.moshi.Moshi;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

public class GithubServiceProvider {

    private static GithubServiceProvider instance;
    private Retrofit retrofit;

    public GithubServiceProvider(String restApiAnonymousBaseUrl) {

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Log.d("TAG", message);
            }
        });
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        GithubInterceptor anonymousInterceptor = new GithubInterceptor();

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .addNetworkInterceptor(anonymousInterceptor)
                .build();

        Moshi jsonConverter = new Moshi.Builder().build();

        retrofit = new Retrofit.Builder()
                .baseUrl(restApiAnonymousBaseUrl)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(MoshiConverterFactory.create(jsonConverter))
                .client(okHttpClient)
                .build();
    }

    public static GithubServiceProvider newInstance(String restApiAnonymousBaseUrl) {
        if(instance == null) {
            instance = new GithubServiceProvider(restApiAnonymousBaseUrl);
        }
        return instance;
    }

    public GithubService get() {
        return retrofit.create(GithubService.class);
    }
}
