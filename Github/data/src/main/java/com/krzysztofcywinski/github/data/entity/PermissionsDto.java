package com.krzysztofcywinski.github.data.entity;

import com.squareup.moshi.Json;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor(suppressConstructorProperties = true)
@Getter
@Setter
public class PermissionsDto {

    @Json(name = "admin")
    private Boolean admin;

    @Json(name = "push")
    private Boolean push;

    @Json(name = "pull")
    private Boolean pull;
}
