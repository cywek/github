package com.krzysztofcywinski.github.data.entity;

import com.krzysztofcywinski.github.domain.model.Repo;
import com.squareup.moshi.Json;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor(suppressConstructorProperties = true)
@Getter
@Setter
public class RepoDto {

    @Json(name = "id")
    private Integer id;

    @Json(name = "owner")
    private OwnerDto owner;

    @Json(name = "name")
    private String name;

    @Json(name = "full_name")
    private String fullName;

    @Json(name = "description")
    private String description;

    @Json(name = "private")
    private Boolean _private;

    @Json(name = "fork")
    private Boolean fork;

    @Json(name = "url")
    private String url;

    @Json(name = "html_url")
    private String htmlUrl;

    @Json(name = "archive_url")
    private String archiveUrl;

    @Json(name = "assignees_url")
    private String assigneesUrl;

    @Json(name = "blobs_url")
    private String blobsUrl;

    @Json(name = "branches_url")
    private String branchesUrl;

    @Json(name = "clone_url")
    private String cloneUrl;

    @Json(name = "collaborators_url")
    private String collaboratorsUrl;

    @Json(name = "comments_url")
    private String commentsUrl;

    @Json(name = "commits_url")
    private String commitsUrl;

    @Json(name = "compare_url")
    private String compareUrl;

    @Json(name = "contents_url")
    private String contentsUrl;

    @Json(name = "contributors_url")
    private String contributorsUrl;

    @Json(name = "deployments_url")
    private String deploymentsUrl;

    @Json(name = "downloads_url")
    private String downloadsUrl;

    @Json(name = "events_url")
    private String eventsUrl;

    @Json(name = "forks_url")
    private String forksUrl;

    @Json(name = "git_commits_url")
    private String gitCommitsUrl;

    @Json(name = "git_refs_url")
    private String gitRefsUrl;

    @Json(name = "git_tags_url")
    private String gitTagsUrl;

    @Json(name = "git_url")
    private String gitUrl;

    @Json(name = "hooks_url")
    private String hooksUrl;

    @Json(name = "issue_comment_url")
    private String issueCommentUrl;

    @Json(name = "issue_events_url")
    private String issueEventsUrl;

    @Json(name = "issues_url")
    private String issuesUrl;

    @Json(name = "keys_url")
    private String keysUrl;

    @Json(name = "labels_url")
    private String labelsUrl;

    @Json(name = "languages_url")
    private String languagesUrl;

    @Json(name = "merges_url")
    private String mergesUrl;

    @Json(name = "milestones_url")
    private String milestonesUrl;

    @Json(name = "mirror_url")
    private String mirrorUrl;

    @Json(name = "notifications_url")
    private String notificationsUrl;

    @Json(name = "pulls_url")
    private String pullsUrl;

    @Json(name = "releases_url")
    private String releasesUrl;

    @Json(name = "ssh_url")
    private String sshUrl;

    @Json(name = "stargazers_url")
    private String stargazersUrl;

    @Json(name = "statuses_url")
    private String statusesUrl;

    @Json(name = "subscribers_url")
    private String subscribersUrl;

    @Json(name = "subscription_url")
    private String subscriptionUrl;

    @Json(name = "svn_url")
    private String svnUrl;

    @Json(name = "tags_url")
    private String tagsUrl;

    @Json(name = "teams_url")
    private String teamsUrl;

    @Json(name = "trees_url")
    private String treesUrl;

    @Json(name = "homepage")
    private String homepage;

    @Json(name = "language")
    private Object language;

    @Json(name = "forks_count")
    private Integer forksCount;

    @Json(name = "stargazers_count")
    private Integer stargazersCount;

    @Json(name = "watchers_count")
    private Integer watchersCount;

    @Json(name = "size")
    private Integer size;

    @Json(name = "default_branch")
    private String defaultBranch;

    @Json(name = "open_issues_count")
    private Integer openIssuesCount;

    @Json(name = "topics")
    private List<String> topics = null;

    @Json(name = "has_issues")
    private Boolean hasIssues;

    @Json(name = "has_wiki")
    private Boolean hasWiki;

    @Json(name = "has_pages")
    private Boolean hasPages;

    @Json(name = "has_downloads")
    private Boolean hasDownloads;

    @Json(name = "pushed_at")
    private String pushedAt;

    @Json(name = "created_at")
    private String createdAt;

    @Json(name = "updated_at")
    private String updatedAt;

    @Json(name = "permissions")
    private PermissionsDto permissions;

    @Json(name = "allow_rebase_merge")
    private Boolean allowRebaseMerge;

    @Json(name = "allow_squash_merge")
    private Boolean allowSquashMerge;

    @Json(name = "allow_merge_commit")
    private Boolean allowMergeCommit;

    @Json(name = "subscribers_count")
    private Integer subscribersCount;

    @Json(name = "network_count")
    private Integer networkCount;

    public static Repo toRepo(RepoDto repoDto) {
        return new Repo(
                repoDto.getId(),
                OwnerDto.toRepoOwner(repoDto.getOwner()),
                repoDto.getName(),
                repoDto.getFullName(),
                repoDto.getDescription(),
                repoDto.get_private(),
                repoDto.getUrl(),
                repoDto.getGitUrl(),
                repoDto.getHomepage(),
                repoDto.getWatchersCount());
    }

    public static List<Repo> toRepo(List<RepoDto> repoDtos) {
        List<Repo> repos = new ArrayList<>();
        for (int i = 0; i < repoDtos.size(); i++) {
            repos.add(RepoDto.toRepo(repoDtos.get(i)));
        }
        return repos;
    }


}
