package com.krzysztofcywinski.github.data.source;

import com.krzysztofcywinski.github.domain.datasource.GithubDataSource;
import com.krzysztofcywinski.github.domain.model.Repo;
import com.krzysztofcywinski.github.domain.model.RepoOwner;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.schedulers.Schedulers;

public class GithubDataSourceImpl implements GithubDataSource {

    public GithubDataSourceImpl() {
    }

    @Override
    public Observable<List<Repo>> getOrganisationRepos(String organisation) {
        List<Repo> repos = new ArrayList<>();
        Repo repo = new Repo(230958,
                new RepoOwner("square", 82592, "https://avatars0.githubusercontent.com/u/82592?v=4"),
                "html5", "square/html5", "A Rails plugin for playing around with HTML5.",
                false, "https://api.github.com/repos/square/html5", "git://github.com/square/html5.git",
                "", 7);

        for (int i = 0; i < 50; i++) {
            repos.add(repo);
        }

        return Observable.just(repos).subscribeOn(Schedulers.io());
    }

}
