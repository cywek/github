package com.krzysztofcywinski.github.injector.component;


import com.krzysztofcywinski.github.injector.scope.PerFragment;
import com.krzysztofcywinski.github.view.fragment.RepoDetailsFragment;
import com.krzysztofcywinski.github.view.fragment.RepoListFragment;

import dagger.Component;

@PerFragment
@Component(dependencies = {AppComponent.class})
public interface FragmentComponent {

    void inject(RepoListFragment fragment);

    void inject(RepoDetailsFragment fragment);

}
