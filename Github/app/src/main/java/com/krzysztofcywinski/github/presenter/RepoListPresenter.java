package com.krzysztofcywinski.github.presenter;

import com.krzysztofcywinski.github.MyApplication;
import com.krzysztofcywinski.github.data.source.GithubDataSourceImpl;
import com.krzysztofcywinski.github.domain.datasource.GithubDataSource;
import com.krzysztofcywinski.github.domain.model.Repo;
import com.krzysztofcywinski.github.injector.scope.PerFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

@PerFragment
public class RepoListPresenter implements RepoListContract.RepoListPresenter {

    public static final int MAX_REPOS = 20;
    private RepoListContract.RepoListPresenterView view;

    @Inject
    MyApplication app;

    private String organisation;
    private List<Repo> reposLimited;
    private CompositeSubscription subscriptions;

    @Inject
    public RepoListPresenter() {
        this.subscriptions = new CompositeSubscription();
    }

    @Override
    public void bindView(RepoListContract.RepoListPresenterView view) {
        this.view = view;
    }

    @Override
    public void init(String organisation) {
        this.organisation = organisation;
        view.showProgressBar();

        GithubDataSource dataSource = new GithubDataSourceImpl();

        Subscription subscribe = dataSource.getOrganisationRepos(organisation)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(repos -> {
                    view.hideProgressBar();
                    reposLimited = repos.subList(0, MAX_REPOS);
                    view.initList(reposLimited);

                }, throwable -> {
                    view.hideProgressBar();
                    view.showUnexpectedError(throwable.toString());
                });

        subscriptions.add(subscribe);

    }


    @Override
    public void retryInit() {
        init(organisation);
    }

    @Override
    public void deinit() {
        subscriptions.unsubscribe();
        view = null;

    }

    @Override
    public void filterResults(String filter) {
        List<Repo> filteredRepos = new ArrayList<>();
        for (int i=0; i < reposLimited.size(); i++) {
            if (reposLimited.get(i).getFullName().contains(filter)) {
                filteredRepos.add(reposLimited.get(i));
            }
        }
        view.setFilteredRepos(filteredRepos);
    }
}
