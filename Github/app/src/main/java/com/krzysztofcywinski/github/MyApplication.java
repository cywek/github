package com.krzysztofcywinski.github;


import android.app.Application;

import com.krzysztofcywinski.github.injector.component.AppComponent;
import com.krzysztofcywinski.github.injector.component.DaggerAppComponent;
import com.krzysztofcywinski.github.injector.module.AppModule;

public class MyApplication extends Application {

    /**
     * Component containing dependencies for whole application lifecycle.
     */
    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        initDaggerComponents();
    }

    private void initDaggerComponents() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
        appComponent.inject(this);
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
