package com.krzysztofcywinski.github.view.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.krzysztofcywinski.github.R;
import com.krzysztofcywinski.github.domain.model.Repo;
import com.krzysztofcywinski.github.injector.scope.PerFragment;
import com.krzysztofcywinski.github.presenter.RepoListContract;
import com.krzysztofcywinski.github.presenter.RepoListPresenter;
import com.krzysztofcywinski.github.view.ReposAdapter;
import com.krzysztofcywinski.github.view.common.BaseFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@PerFragment
public class RepoListFragment extends BaseFragment implements RepoListContract.RepoListPresenterView {

    private static final String TAG = "RepoListFragment";
    public static final String KEY_ORGANISTAION = "Organistaion";

    @BindView(R.id.reposList)
    RecyclerView reposList;

    @BindView(R.id.noReposInfo)
    RelativeLayout noReposInfo;

    @BindView(R.id.repoFilter)
    TextInputEditText repoFilter;

    private View rootView;
    private Unbinder unbinder;
    private ReposAdapter reposAdapter;

    private ProgressDialog progressDialog;


    public interface OnRepoClickedListener {
        void onRepoClicked(Repo repo);
    }

    public void setOnRepoClickedListener(OnRepoClickedListener onRepoClickedListener) {
        this.onRepoClickedListener = onRepoClickedListener;
    }

    private OnRepoClickedListener onRepoClickedListener;

    @Inject
    RepoListPresenter presenter;

    public RepoListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_repo_list, container, false);

        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initDaggerComponents();
        initViews();

        if (getArguments() != null) {
            Bundle bundle = getArguments();
            final String organisation = bundle.getString(KEY_ORGANISTAION);
            if (organisation != null) {
                presenter.init(organisation);
            }
        }

        repoFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                presenter.filterResults(editable.toString());
            }
        });
    }

    public void repoClicked(Repo repo) {
        if (onRepoClickedListener != null) {
            onRepoClickedListener.onRepoClicked(repo);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.deinit();
        unbinder.unbind();
    }

    @Override
    protected void initDaggerComponents() {
        FragmentComponentInitializer.init().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.bindView(this);
    }

    @Override
    public void initList(List<Repo> repos) {

        if (repos != null && repos.size() > 0) {

            reposAdapter = new ReposAdapter(repos);
            reposList.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
            reposList.setAdapter(reposAdapter);
            Observable<Repo> observable = reposAdapter.getPositionClicks();
            observable.observeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::repoClicked);

        } else {
            noReposInfo.setVisibility(View.VISIBLE);
            repoFilter.setVisibility(View.GONE);
        }

    }

    @Override
    public void showUnexpectedError(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.error_title).setMessage(message).setPositiveButton(R.string.retry, (dialogInterface, i) -> {
            presenter.retryInit();
        }).setNegativeButton(android.R.string.cancel, (dialogInterface, i) -> {

        }).show();
    }

    @Override
    public void setFilteredRepos(List<Repo> filteredRepos) {
        reposAdapter.changeRepoSet(filteredRepos);
    }

    @Override
    public void showProgressBar() {

        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        progressDialog = ProgressDialog.show(
                getActivity(), getString(R.string.loading_repos), getString(R.string.please_wait),
                true);
        progressDialog.setCancelable(false);
    }


    @Override
    public void hideProgressBar() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public static RepoListFragment getIntance(String organisation) {
        RepoListFragment fragment = new RepoListFragment();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_ORGANISTAION, organisation);
        fragment.setArguments(bundle);
        return fragment;
    }
}