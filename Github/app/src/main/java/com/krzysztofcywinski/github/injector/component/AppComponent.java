package com.krzysztofcywinski.github.injector.component;


import com.krzysztofcywinski.github.MyApplication;
import com.krzysztofcywinski.github.injector.module.AppModule;
import com.krzysztofcywinski.github.injector.scope.PerApp;

import dagger.Component;

@PerApp
@Component(modules = {AppModule.class})
public interface AppComponent {

    /**
     *
     * @return Application dependency.
     */
    MyApplication app();

    /**
     * Injects dependencies into {@link MyApplication}
     *
     * @param app {@link MyApplication}
     */
    void inject(MyApplication app);
}
