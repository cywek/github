package com.krzysztofcywinski.github.view.common;


import android.app.Fragment;

import com.krzysztofcywinski.github.MyApplication;
import com.krzysztofcywinski.github.injector.component.DaggerFragmentComponent;
import com.krzysztofcywinski.github.injector.component.FragmentComponent;

public abstract class BaseFragment extends Fragment {

    /**
     * Initializer for {@link FragmentComponent}
     */
    protected static class FragmentComponentInitializer {

        private FragmentComponentInitializer() {
        }

        /**
         * Init injector component.
         *
         * @return {@link FragmentComponent}
         */
        public static FragmentComponent init() {
            return DaggerFragmentComponent.builder()
                    .appComponent(MyApplication.getAppComponent())
                    .build();
        }
    }

    /**
     * Initialize dagger component
     */
    protected abstract void initDaggerComponents();

    /**
     * Initialize views
     */
    protected abstract void initViews();

}