package com.krzysztofcywinski.github.presenter;

import com.krzysztofcywinski.github.domain.model.Repo;

import java.util.List;

public interface RepoListContract {

        interface RepoListPresenterView {

            void showProgressBar();

            void hideProgressBar();

            void initList(List<Repo> repos);

            void showUnexpectedError(String message);

            void setFilteredRepos(List<Repo> filteredRepos);
        }

        interface RepoListPresenter extends Presenter<RepoListContract.RepoListPresenterView>{

            void init(String organisation);

            void deinit();

            void filterResults(String filter);

            void retryInit();
        }
}
