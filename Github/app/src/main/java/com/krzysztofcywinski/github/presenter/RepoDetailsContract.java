package com.krzysztofcywinski.github.presenter;

import com.krzysztofcywinski.github.domain.model.Repo;

public interface RepoDetailsContract {

    interface RepoDetailsPresenterView {

        void setRepoName(String repoName);

        void setRepoDescription(String repoDescription);

        void setRepoPrivate(boolean isPrivate);

        void setOwnerLogin(String ownerLogin);

        void setOwnerAvatar(String avatarUrl);

        void setWatchersCount(int watchersCount);

    }

    interface RepoDetailsPresenter extends Presenter<RepoDetailsPresenterView>{

        void switchRepo(Repo repo);

        void deinit();

    }

}
