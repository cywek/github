package com.krzysztofcywinski.github.presenter;

public interface Presenter<V> {

    /**
     * Bind view to control by presenter.
     *
     * @param view View to control by presenter.
     */
    void bindView(V view);
}
