package com.krzysztofcywinski.github.presenter;


import android.support.annotation.NonNull;

import com.krzysztofcywinski.github.domain.model.Repo;
import com.krzysztofcywinski.github.domain.model.RepoOwner;
import com.krzysztofcywinski.github.injector.scope.PerFragment;

import javax.inject.Inject;

@PerFragment
public class RepoDetailsPresenter implements RepoDetailsContract.RepoDetailsPresenter {

    private RepoDetailsContract.RepoDetailsPresenterView view;

    @Inject
    public RepoDetailsPresenter() {
    }

    @Override
    public void bindView(RepoDetailsContract.RepoDetailsPresenterView view) {
        this.view = view;
    }

    @Override
    public void switchRepo(@NonNull Repo repo) {
        view.setRepoName(repo.getName());
        view.setRepoDescription(repo.getDescription());
        view.setWatchersCount(repo.getWatchersCount());

        RepoOwner repoOwner = repo.getOwner();
        view.setOwnerLogin(repoOwner.getLogin());
        view.setOwnerAvatar(repoOwner.getAvatarUrl());
        view.setRepoPrivate(repo.get_private());

    }

    public void deinit() {
        view = null;
    }
}
