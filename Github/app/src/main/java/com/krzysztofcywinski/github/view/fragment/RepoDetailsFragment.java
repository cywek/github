package com.krzysztofcywinski.github.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.krzysztofcywinski.github.R;
import com.krzysztofcywinski.github.domain.model.Repo;
import com.krzysztofcywinski.github.injector.scope.PerFragment;
import com.krzysztofcywinski.github.presenter.RepoDetailsContract;
import com.krzysztofcywinski.github.presenter.RepoDetailsPresenter;
import com.krzysztofcywinski.github.view.common.BaseFragment;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

@PerFragment
public class RepoDetailsFragment extends BaseFragment implements RepoDetailsContract.RepoDetailsPresenterView {

    private static final String KEY_REPO = "Repo";

    @BindView(R.id.avatar)
    ImageView avatar;

    @BindView(R.id.repoName)
    TextView repoName;

    @BindView(R.id.ownerName)
    TextView ownerName;

    @BindView(R.id.repoDescription)
    TextView repoDescription;

    @BindView(R.id.watchers)
    TextView watchers;

    @BindView(R.id.isPrivate)
    ImageView isPrivate;

    private View rootView;
    private Unbinder unbinder;

    @Inject
    RepoDetailsPresenter presenter;

    public RepoDetailsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_repo_details, container, false);

        unbinder = ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initDaggerComponents();
        initViews();

        if (getArguments() != null) {
            Bundle bundle = getArguments();
            Repo repo = bundle.getParcelable(KEY_REPO);
            presenter.switchRepo(repo);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.deinit();
        unbinder.unbind();
    }

    @Override
    protected void initDaggerComponents() {
        FragmentComponentInitializer.init().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.bindView(this);
    }

    public static RepoDetailsFragment getInstance(final Repo repo) {
        RepoDetailsFragment repoDetailsFragment = new RepoDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_REPO, repo);
        repoDetailsFragment.setArguments(bundle);
        return repoDetailsFragment;
    }

    public void switchRepo(Repo repo) {
        presenter.switchRepo(repo);
    }

    @Override public void setRepoName(final String repoName) {
        this.repoName.setText(repoName);
    }

    @Override public void setRepoDescription(final String repoDescription) {
        this.repoDescription.setText(repoDescription);
    }

    @Override public void setRepoPrivate(final boolean isPrivate) {
        int drawable = isPrivate ? android.R.drawable.ic_secure : android.R.drawable.ic_partial_secure;
        this.isPrivate.setImageResource(drawable);
    }

    @Override public void setOwnerLogin(final String ownerLogin) {
        ownerName.setText(ownerLogin);
    }

    @Override public void setOwnerAvatar(final String avatarUrl) {

        Picasso.with(getActivity().getApplicationContext())
                .load(avatarUrl)
                .fit()
                .placeholder(R.drawable.ph_user)
                .into(avatar);
    }

    @Override public void setWatchersCount(final int watchersCount) {
        watchers.setText(""+watchersCount);
    }
}