package com.krzysztofcywinski.github.view.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;

import com.krzysztofcywinski.github.R;
import com.krzysztofcywinski.github.domain.model.Repo;
import com.krzysztofcywinski.github.model.AppState;
import com.krzysztofcywinski.github.view.fragment.RepoDetailsFragment;
import com.krzysztofcywinski.github.view.fragment.RepoListFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RepoActivity extends AppCompatActivity {

    @BindView(R.id.mainContainer)
    RelativeLayout mainContainer;

    private RepoDetailsFragment repoDetailsFragment;
    private RepoListFragment squareReposFragment;

    AppState appState = AppState.REPO_LIST;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repos);
        ButterKnife.bind(this);

        squareReposFragment = RepoListFragment.getIntance("square");
        squareReposFragment.setOnRepoClickedListener(repo -> RepoActivity.this.onRepoClicked(repo));
        createFragment(squareReposFragment);
    }

    private void onRepoClicked(final Repo repo) {
        squareReposFragment.getView().setVisibility(View.GONE);
        appState = AppState.REPO_DETAILS;
        if (repoDetailsFragment == null) {
            repoDetailsFragment = RepoDetailsFragment.getInstance(repo);
            createFragment(repoDetailsFragment);
        } else {
            repoDetailsFragment.switchRepo(repo);
            repoDetailsFragment.getView().setVisibility(View.VISIBLE);
        }
        hideKeyboard();

    }

    private void hideKeyboard() {
        View view = squareReposFragment.getView();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void createFragment(Fragment fragment) {
        FragmentManager fragMan = getFragmentManager();
        FragmentTransaction fragTransaction = fragMan.beginTransaction();
        fragTransaction.add(mainContainer.getId(), fragment, fragment.getClass().getSimpleName());
        fragTransaction.commit();
        fragTransaction.show(fragment);
    }

    @Override
    public void onBackPressed() {
        switch (appState) {

            case REPO_LIST:
                super.onBackPressed();
                break;
            case REPO_DETAILS:
                appState = AppState.REPO_LIST;
                repoDetailsFragment.getView().setVisibility(View.GONE);
                squareReposFragment.getView().setVisibility(View.VISIBLE);
                break;
        }

    }

}
