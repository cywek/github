package com.krzysztofcywinski.github.model;

public enum AppState {
    REPO_LIST, REPO_DETAILS
}
