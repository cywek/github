package com.krzysztofcywinski.github.injector.module;

import com.krzysztofcywinski.github.MyApplication;
import com.krzysztofcywinski.github.injector.scope.PerApp;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private final MyApplication application;

    public AppModule(MyApplication application) {
        this.application = application;
    }

    @PerApp
    @Provides
    public MyApplication provideApp() {
        return application;
    }
}
