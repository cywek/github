package com.krzysztofcywinski.github.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.krzysztofcywinski.github.R;
import com.krzysztofcywinski.github.domain.model.Repo;

import java.util.List;

import rx.Observable;
import rx.subjects.PublishSubject;


public class ReposAdapter extends RecyclerView.Adapter<ReposAdapter.MyViewHolder> {

    private List<Repo> repos;
    private final PublishSubject<Repo> onClickSubject = PublishSubject.create();

    public ReposAdapter(List<Repo> repos) {
        this.repos = repos;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_element_repo, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.setRepo(repos.get(position));
        holder.repoName.setText(repos.get(position).getFullName());

    }

    @Override
    public int getItemCount() {
        return repos.size();
    }

    public void changeRepoSet(List<Repo> filteredRepos) {
        repos = filteredRepos;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView repoName;
        private Repo repo;

        public MyViewHolder(View view) {
            super(view);
            repoName = view.findViewById(R.id.repoName);
            view.setOnClickListener(view1 -> onClickSubject.onNext(repo));
        }

        public void setRepo(Repo repo) {
            this.repo = repo;
        }
    }

    public Observable<Repo> getPositionClicks() {
        return onClickSubject.asObservable();
    }

}
